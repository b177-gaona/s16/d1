// Repetition Control Structures

// While Loop
/* Synatx:
	while(condition){
		statement/s;
	}

*/

let count = 5;

// count = 5
while(count !== 0) {
	console.log("While: " + count);
	count --;
}

console.log("Displays numbers 1-10");
count = 1;

while (count < 11) {
	console.log("While: " + count);
	count ++;
}

// Do while loop
/* Syntax:
	do {
		statement;
	} while (condition);
*/

// Number() is similar to parseInt when converting String into numbers
let number = Number(prompt("Give me a number"));

do {
	console.log("Do While: " + number);
	number += 1;
} while (number < 10);

// Create a new variable to be used in displaying even numbers from 2-10 using while loop.
numTest = 2

while (numTest<=10) {
	console.log ("Even Number: " + numTest);
	numTest += 2;
};

// For Loop
/* Syntax:
	for (initialization; condition; stepExpression) {
		statement;
	}

*/
console.log("For Loop");

for (let count2 = 0; count2 <= 20; count2++) {
	console.log(count2);
};

console.log("Even For Loop");
let even = 2;
for (let counter = 1; counter <=5; counter++) {
	console.log("Even: " + even);
	even += 2;
}
/* Expected output:
	Even: 2
	Even: 4
	Even: 6
	Even: 8
	Even: 10
*/

let myString = 'alex';
// .length property is used to count the characters in a string
console.log(myString.length);

console.log(myString[0]);
console.log(myString[3]);

// myString.length = 4
for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

for (let x = (myString.length-1) ; x >= 0; x--) {
	console.log(myString[x]);
}

// Print out letter individually but will print 3 instead of the vowels
let myName = "AlEx";

for (let i = 0; i < myName.length; i++) {
	if(
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u' 
	){
		console.log(3);
	}
}

// Continue & Break Statements

for (let count = 0; count <= 20; count++) {
	// if remainder is equal to 0, tells the code to continue to iterate
	if (count % 2 === 0) {
		continue;
	}
	console.log("Continue and Break: " + count);
	// if the current value of count is greater than 10, tells the code to stop the loop
	if (count > 10) {
		break;
	}
}

let name = 'alexandro';
for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if(name[i].toLowerCase()==="a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] === "d") {
		break;
	}
}

// Activity

let tNumber = Number(prompt("Give me a number"));
console.log ("The number you provided is " +  tNumber);

for (let i=tNumber; i >= 0; i--) {
	console.log(tNumber + " " + i);

	if(tNumber % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(tNumber % 5 === 0) { 
		console.log(tNumber);
		continue;
	}

	if (tNumber <= 50) {
		break;
	}
}

let tString = "supercalifragilisticexpialidocious"
console.log(tString + tString.length);
let tStringVowels = '';
let tStringConsonants = '';

for (let i = 0; i < tString.length; i++) {
	if(
		tString[i].toLowerCase() == 'a' ||
		tString[i].toLowerCase() == 'e' ||
		tString[i].toLowerCase() == 'i' ||
		tString[i].toLowerCase() == 'o' ||
		tString[i].toLowerCase() == 'u' 
	) {
		continue;
		// tStringVowels= tStringVowels + tString[i];	
		// console.log(tStringVowels.length + "_" + tString[i] + "_" + tStringVowels);
	}
	else {
		
		tStringConsonants= tStringConsonants + tString[i];
		console.log(tStringConsonants.length + tString[i] + tStringConsonants);
	}
}

console.log(tStringVowels);
console.log(tStringConsonants);
